/*
  elm.cpp - Library for communicating with an ELM327 Shield.
  Created by Florian Beck, Viktor Pavlovic, Simon Schuster.
  Hochschule der Medien, Stuttgart.
  Stuttgart Media University
*/

#ifndef ELM_cpp
#define ELM_cpp

#include "elm.h"
#include <sstream>


bool begins_with(const char* base, const char* str)
{
    return (strstr(base, str) - base) == 0;
}

bool ends_with(const char* base, const char* str) {
    int blen = strlen(base);
    int slen = strlen(str);
    return (blen >= slen) && (0 == strcmp(base + blen - slen, str));
}

inline std::string format_binary(unsigned int x)
{
    static char b[33];
    b[32] = '\0';

    for (int z = 0; z < 32; z++) {
        b[31-z] = ((x>>z) & 0x1) ? '1' : '0';
    }

    return b;
}

/* --- PRINTF_BYTE_TO_BINARY macro's --- */
#define PRINTF_BINARY_PATTERN_INT8 "%c%c%c%c%c%c%c%c"
#define PRINTF_BYTE_TO_BINARY_INT8(i)    \
    (((i) & 0x80ll) ? '1' : '0'), \
    (((i) & 0x40ll) ? '1' : '0'), \
    (((i) & 0x20ll) ? '1' : '0'), \
    (((i) & 0x10ll) ? '1' : '0'), \
    (((i) & 0x08ll) ? '1' : '0'), \
    (((i) & 0x04ll) ? '1' : '0'), \
    (((i) & 0x02ll) ? '1' : '0'), \
    (((i) & 0x01ll) ? '1' : '0')

#define PRINTF_BINARY_PATTERN_INT16 \
    PRINTF_BINARY_PATTERN_INT8              PRINTF_BINARY_PATTERN_INT8
#define PRINTF_BYTE_TO_BINARY_INT16(i) \
    PRINTF_BYTE_TO_BINARY_INT8((i) >> 8),   PRINTF_BYTE_TO_BINARY_INT8(i)
#define PRINTF_BINARY_PATTERN_INT32 \
    PRINTF_BINARY_PATTERN_INT16             PRINTF_BINARY_PATTERN_INT16
#define PRINTF_BYTE_TO_BINARY_INT32(i) \
    PRINTF_BYTE_TO_BINARY_INT16((i) >> 16), PRINTF_BYTE_TO_BINARY_INT16(i)
#define PRINTF_BINARY_PATTERN_INT64    \
    PRINTF_BINARY_PATTERN_INT32             PRINTF_BINARY_PATTERN_INT32
#define PRINTF_BYTE_TO_BINARY_INT64(i) \
    PRINTF_BYTE_TO_BINARY_INT32((i) >> 32), PRINTF_BYTE_TO_BINARY_INT32(i)
/* --- end macros --- */

// #include <stdio.h>
// int main() {
//     long long int flag = 1648646756487983144ll;
//     printf("My Flag "
//            PRINTF_BINARY_PATTERN_INT64 "\n",
//            PRINTF_BYTE_TO_BINARY_INT64(flag));
//     return 0;
// }


void ELM::flush() { 
	while(UART->readable()) {
		UART->getc();
	}
}

void ELM::qAT(const char *msg)
{
	char r;
	uint32_t i = 0; //received characters  
	char response[128];

	UART->puts(msg); 
	r = 0;
	while ((r!=62) && (i < sizeof(response))) { // response not complete
		response[i++] = r =UART->getc();  
	}
}

void ELM::init() {

	flush();

	// Echo On
	qAT("AT E1\r"); 

	// Headers On
	qAT("AT H1\r"); 

	// Adaptive Timing auto1 
	qAT("AT AT1\r"); 

	// Max Timeout
	qAT("AT ST FF\r");   
}


bool ELM::protocol(protocol_t p) {
	char cmd[7] = "ATTPA";
	cmd[5] = p;
	cmd[6] = 0;
	if (begins_with(AT(cmd), "OK")) {
		return true;
	} else {
		return false;
	}
}

/*
 * Reset ELM Chip
 *
 */
bool ELM::reset() { 
	flush();
	if (begins_with(AT("ATZ"), "ELM327")) {
		return true;
	} else {
		return false;
	}
}

/*
 * Return comma-separated list of available PIDs
 *
 */
std::string ELM::get_available_pids() { 
	if (!available_pids_checked) {
		update_available_pids();
	}
	std::ostringstream data;
	bool first = true;
	for (uint32_t i = 0; i <= 255; i++) {
		if (available_pids[i]) {
			if (first) {first = false;} else {data << ",";}
			data << i;
		}
	}
	return data.str();
}

/*
 * Check if specific PID is available
 *
 */
bool ELM::pid_available(uint8_t pid) { 
	if (!available_pids_checked) {
		update_available_pids();
	}
	return available_pids[pid];
}

/*
 * Get specific PIDs raw data
 *
 */
const char* ELM::get_pid_rawdata(uint8_t id) {  
	if (!available_pids_checked) {
		update_available_pids();
	}
	if (!pid_available(id)) {
		return "ERROR: pid not available";
	}
	char* data = (char*)AT(pid(id));
	char hex[6];
	sprintf(hex, "41 %0x", id);
	if (begins_with(data, hex)) {
		data += 6;
	}
	if (ends_with(data, " ")) {
		data[strlen(data)-1] = 0;
	}
	return data;
}

/*
 * Get specific PIDs parsed data
 *
 */
const char* ELM::get_pid_data(uint8_t id) {
	auto rawdata = get_pid_rawdata(id);
	
	#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
		char* myVal =  "";
		char* myUnit = "";
		char* myDesc = "";
		
		uint8_t data_length = ((rawdata.length()+1)/3);
		uint8_t data[data_length];

		for (uint32_t i = 0; i < data_length; i++) {
			data[i] = hex2uint8_t(rawdata,i," ");
		}

		parsePID(id, data, &myVal, &myUnit, &myDesc);
		return myVal;
	#else 
		return rawdata;
	#endif
}

/*
 * Get specific PIDs unit
 *
 */
const char* ELM::get_pid_unit(uint8_t id) {
	auto rawdata = get_pid_rawdata(id);
	
	#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
		char* myVal =  "";
		char* myUnit = "";
		char* myDesc = "";
		
		uint8_t data_length = ((rawdata.length()+1)/3);
		uint8_t data[data_length];

		for (uint32_t i = 0; i < data_length; i++) {
			data[i] = hex2uint8_t(rawdata,i," ");
		}

		parsePID(id, data, &myVal, &myUnit, &myDesc);
		return myUnit;
	#else 
		return "ERROR: parsing of pid data not available on this microcontroller";
	#endif
}

/*
 * Get a specific PIDs description
 *
 */
const char* ELM::get_pid_desc(uint8_t id) {
	auto rawdata = get_pid_rawdata(id);
	
	#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
		char* myVal =  "";
		char* myUnit = "";
		char* myDesc = "";
		
		uint8_t data_length = ((rawdata.length()+1)/3);
		uint8_t data[data_length];

		for (uint32_t i = 0; i < data_length; i++) {
			data[i] = hex2uint8_t(rawdata,i," ");
		}

		parsePID(id, data, &myVal, &myUnit, &myDesc);
		return myDesc;
	#else 
		return "ERROR: parsing of pid data not available on this microcontroller";
	#endif
}

/*
 * Get the vehicle identification number (vin)
 *
 */
const char* ELM::get_vin() { // tested (without vehicle), response is NO DATA, works
	return AT("0902");
}

/*
 * Get the ecu name
 *
 */
const char* ELM::get_ecu() { // tested (without vehicle), response is NO DATA, works
	return AT("090A");
}

/*
 * Get the cars onboard voltage
 *
 */
const char* ELM::get_voltage() { 
	return AT("ATRV");
}

/*
 * Get the OBD protocol
 *
 */
const char* ELM::get_protocol() { 
	auto data = AT("ATDP");
	if (begins_with(data, "AUTO")) {
		data += 6;
	}
	return data;
}

/*
 * Get the stored trouble codes
 *
 */
const char* ELM::get_dtc() {  // tested, no parsing of error codes, works
	char* data = (char*)AT("03");
	if (begins_with(data, "43")) {
		data += 3;
	}
	if (ends_with(data, " ")) {
		data[strlen(data)-1] = 0;
	}
	return data;
}

/*
 * Clear trouble codes and malfunction indicator lamp (mil)
 *
 */
bool ELM::clear_dtc() { // tested (without present dtc's), works
	if (begins_with(AT("04"), "44")) {
		return true;
	} else {
		return false;
	}
}


/*
 * Build AT command to get current data by specified PID
 *
 */
char* ELM::pid(uint8_t id) {
	static char ret[5] = {0};
	sprintf(ret, "01%0x", id);
	return ret;
}

 
/*
 * Convert hex string to uint8_t
 *
 */ 
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
uint8_t ELM::hex2uint8_t(char* in, uint8_t pos, char* separator) {
	uint32_t seplen = separator.length();
	char* part = in.substr(pos*(2+seplen),pos*(2+seplen)+2);
	// char partchar[3];
	// part.toCharArray(partchar, 3);
	unsigned long l = strtoul(part.c_str(),NULL,16);
	return l & 0xFF;
}
#endif
	
/*
 * Update the list of supported PIDs
 *
 */
void ELM::update_available_pids(){
	
	// initialize supported pid list
	for (uint32_t h = 0; h < 256; h++) {
		available_pids[h] = false;
	}	
	available_pids[0] = true; // PID0 is always supported and can't be checked for support
	
	
	update_available_pidset(1);
	
	
	// Check if pid 0x20 is available (meaning next set is supported)
	if ( available_pids[0x20] ) {
		
		update_available_pidset(2);
	
		if ( available_pids[0x40] ) {
		
			update_available_pidset(3);
		
			if ( available_pids[0x60] ) {
		
				update_available_pidset(4);
				
				if ( available_pids[0x80] ) {
		
					update_available_pidset(5);
					
					if ( available_pids[0xA0] ) {
		
						update_available_pidset(6);
						
						if ( available_pids[0xC0] ) {
		
							update_available_pidset(7);
							
						}
					}
				}
			}	
		}	
	}

	available_pids_checked = true;
	
	
}

/*
 * Update supported PIDs defined by set (1-7)
 *
 */
void ELM::update_available_pidset(uint8_t set) {
	
	const char* cmd1;
	
	// Select command
	switch (set) {
			case 1:
				cmd1 = "0100";
				break;
			case 2:
				cmd1 = "0120";
				break;
			case 3:
				cmd1 = "0140";
				break;
			case 4:
				cmd1 = "0160";
				break;
			case 5:
				cmd1 = "0180";
				break;
			case 6:
				cmd1 = "01A0";
				break;
			case 7:
				cmd1 = "01C0";
				break;	
			default:
				cmd1 = "0100";
				break;
	}	
	
	// Get first set of pids
	auto seq1 = AT(cmd1);
	
	
	// trim to continuous 32bit hex string
	char part1[9] = {0};
	memcpy(&part1[0], &seq1[6], 2);
	memcpy(&part1[2], &seq1[9], 2);
	memcpy(&part1[4], &seq1[12], 2);
	memcpy(&part1[6], &seq1[15], 2);
	
	// char p1char[part1.length() + 1];
	// part1.toCharArray(p1char, part1.length() + 1);
	
	// convert to long
	uint32_t l1 = strtoul(part1,NULL,16);
	//convert to binary string
	char bin1[33] = {0};  //format_binary(l1);
	sprintf(bin1, PRINTF_BINARY_PATTERN_INT32, PRINTF_BYTE_TO_BINARY_INT32(l1));

	uint32_t m = (set-1) * 32;
	
	// fill supported pid list
	for (uint32_t i = 0; i <= 32; i++) {

		if (bin1[i] == '0') {
			available_pids[i+m] = false;
		} else {
			available_pids[i+m] = true;
			//Serial.print(char*(i+m,DEC) + " ");//DEBUG
		}		
	}
}	

/*
 * ELM327 command and response handler
 *
 */
const char* ELM::AT(const char* Cmd)
{
	char r;
	volatile uint32_t i = 0; //received characters  
	char send[128] = {0};
	static char response[128];
	uint32_t len = strlen(Cmd);

	if (len >= sizeof(response)) {
		// Buffer needs to be increased
		__BKPT('0');
	}
  
	flush();

	// send to elm
	__BKPT('0');
	i = 0;
	r = 0;
	strcpy(send, Cmd);
	send[len] = '\r';
	send[len+1] = 0;
	UART->puts(send);  

	r = 0;
	while ((r!=62) && (i < sizeof(response)) && (i < strlen(send))) {
		response[i++] = r =UART->getc();  
	}

	send[len] = 0;
	if (strcmp(response, send)) {
		__BKPT('0');
	}

  //wait for response and process received data
  unsigned long timestamp = time(NULL); //set timestamp for timeout
//   char* Response;
  while(true) { //listen on serial port until response is complete
    if (UART->readable()) {
        r = UART->getc();
        if (r!=62) { // response not complete
          if (r>=32) { //don't use control characters for response string
            response[i++] = r;
			if (i == sizeof(response)) {
	        	__BKPT('0');
				return "ERROR: response buffer too small"; // error message
			}
          }
        } 
        else { // response complete
          response[i] = 0;
          __BKPT('0');
		  return response;
        }
    //   }
    }
    //check for timeout
    // if ((unsigned long)(time(NULL)-timestamp)>5000) {
    //   __BKPT('0');
	//   return "ERROR: ELM timeout"; // error message
    //   break;
    // }
  }
}


void calc_percent(uint8_t in, char *val, char *unit) {
	
	sprintf(val, "%0.2f", in*100.0/255.0);
	strcpy(unit, "%");
	
}

void calc_temp(uint8_t in, char *val, char *unit) {
	
	sprintf(val, "%0.2f", in-40.0);
	strcpy(unit, "�C");
	
}

void calc_os_val(uint8_t in1, uint8_t in2, char *val, char *unit) {
	sprintf(val, "%0.2f,%0.2f", (in1 / 200.0),  (in2 - 128.0) * 100.0 / 128.0 );
	strcpy(unit, "V, %");
}

void calc_o2s(uint8_t in1, uint8_t in2, uint8_t in3, uint8_t in4, char *val, char *unit) {
	sprintf(val, "%0.2f,%0.2f", ((in1*256.0) + in2) * 2.0 / 65535.0,  ((in3*256.0) + in4) * 8.0 / 65535.0 ) ;
	strcpy(unit, "n/a , V");
}

void calc_o2s_b(uint8_t in1, uint8_t in2, uint8_t in3, uint8_t in4, char *val, char *unit) {
	sprintf(val, "%0.2f,%0.2f", ((in1*256.0) + in2) * 2.0 / 65535.0, (((in3*256.0) + in4) / 256.0) - 128.0 ) ; 
	strcpy(unit, "n/a , mA"); 
}

void calc_stsos(uint8_t in1, uint8_t in2, char *val, char *unit) {
	sprintf(val, "%0.2f,%0.2f", (in1-128.0)*100.0/128.0, (in2-128.0)*100.0/128.0); 
	strcpy(unit, "%, %");
}


void calc_cat_temp(uint8_t in1, uint8_t in2, char *val, char *unit) {
	sprintf(val, "%d", (((in1*256)+in2)/10) - 40 ); 
	strcpy(unit, "�C"); 	
}

/*
 * Return parsed value, unit and PID description from supported raw data
 *
 */
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
void ELM::parsePID(uint8_t pid, uint8_t data[], char *value, char *unit, char *desc) {
	
	#define A data[0]
	#define B data[1]
	#define C data[2]
	#define D data[3]
	#define E data[4]
	
	#define CALC_PERCENT \
		*value=std::to_string(data[0]*100.0/255.0); \
		*unit="%";
		
	#define CALC_TEMP \
		*value=std::to_string(data[0]-40.0); \
		*unit="�C";
		
	// Helper Variables
	char* value_three_tmp;
	char* temp1C;	
	
  switch (pid) {
	case 0x03: // Fuel system status (BIT ENCODED)
		
		switch (A) { // Fuel system #1
			case 1:
				value_three_tmp=char*("Open loop due to insufficient engine temperature");
				break;
			case 2:
				value_three_tmp=char*("Closed loop, using oxygen sensor feedback to determine fuel mix");
				break;
			case 4:
				value_three_tmp=char*("Open loop due to engine load OR fuel cut due to deceleration");
				break;
			case 8:
				value_three_tmp=char*("Open loop due to system failure");
				break;
			case 16:
				value_three_tmp=char*("Closed loop, using at least one oxygen sensor but there is a fault in the feedback system");
				break;
			default:
				value_three_tmp=char*("ERROR - INVALID VALUE");
				break;
		}
		switch (B) { // Fuel system #2 (if present)
			case 0:
				break;
			case 1:
				value_three_tmp = "Fuel System 1: " + value_three_tmp + char*(", Fuel System 2: Open loop due to insufficient engine temperature");
				break;
			case 2:
				value_three_tmp = "Fuel System 1: " + value_three_tmp + char*(", Fuel System 2: Closed loop, using oxygen sensor feedback to determine fuel mix");
				break;
			case 4:
				value_three_tmp = "Fuel System 1: " + value_three_tmp + char*(", Fuel System 2: Open loop due to engine load OR fuel cut due to deceleration");
				break;
			case 8:
				value_three_tmp = "Fuel System 1: " + value_three_tmp + char*(", Fuel System 2: Open loop due to system failure");
				break;
			case 16:
				value_three_tmp = "Fuel System 1: " + value_three_tmp + char*(", Fuel System 2: Closed loop, using at least one oxygen sensor but there is a fault in the feedback system");
				break;
			default:
				value_three_tmp = "Fuel System 1: " + value_three_tmp + char*(", Fuel System 2: ERROR - INVALID VALUE");
				break;	
		}	
		*value = value_three_tmp;
		*unit = "";
		*desc = "Fuel system status";
		break;
    case 0x04: //Calculated engine load value
      calc_percent(A, value, unit);
      *desc="Calculated engine load value";
      break;
    case 0x05: //Engine coolant temperature
      calc_temp(A, value, unit);
      *desc="Engine coolant temperature";
      break;
	#define CALC_STFT \
      *value=std::to_string((data[0]-128.0)*100.0/128.0); \
      *unit="%";
    case 0x06:
		CALC_STFT
		*desc="Short term fuel % trim - Bank 1";
		break;
    case 0x07:
		CALC_STFT
		*desc="Long term fuel % trim - Bank 1";
		break;
    case 0x08:
		CALC_STFT
		*desc="Short term fuel % trim - Bank 2";
		break;
    case 0x09:
		CALC_STFT
		*desc="Long term fuel % trim - Bank 2";
		break;
    case 0x0A:
		*value=std::to_string(data[0]*3);
		*unit="kPa";
		*desc="Fuel pressure";
		break;
    case 0x0B:
		*value=std::to_string(data[0]);
		*unit="kPa";
		*desc="Intake manifold absolute pressure";
		break;
	case 0x0C:
		*value=std::to_string(((data[0]*256.0)+data[1])/4.0);
		*unit="rpm";
		*desc="Engine RPM";
		break;
	case 0x0D:
	  *value=std::to_string(data[0]);
	  *unit="km/h";
	  *desc="Vehicle speed";
	  break;
	case 0x0E:
	  *value=std::to_string((data[0]-128.0)/2.0);
	  *unit="deg";
	  *desc="Timing advance";
	  break;
	case 0x0F:
	  calc_temp(A, value, unit);
	  *desc="Intake air temperature";
	  break;
	case 0x10:
	  *value=std::to_string( ((data[0]*256.0) + data[1])/100.0 );
	  *unit="g/s";
	  *desc="MAF air flow rate";
	  break;
	case 0x11:
	  calc_percent(A, value, unit);
	  *desc="Throtle position";
	  break;
	case 0x12:
		switch(data[0]) {
			case 1:
				*value=char*("Upstream");
				break;
			case 2:
				*value=char*("Downstream of catalytic converter");
				break;
			case 4:
				*value=char*("From the outside atmosphere or off");
				break;
			case 8:
				*value=char*("Pump commanded on for diagnostics");
				break;
			default:
				*value=char*("ERROR - INVALID VALUE");
				break;
		}
	  
	  *unit="";
	  *desc="Commanded secondary air status";
	  break;
	case 0x13:
		*value=std::to_string(data[0]); //BIT ENCODED TODO: DECODE
		*unit="";
		*desc="Oxygen sensors present";
	  break;
	 #define CALC_OS_VAL \
	  *value=std::to_string( (data[0] / 200.0)  ) + "," + std::to_string( (data[1] - 128.0) * 100.0 / 128.0 ); \
	  *unit="V, %";
	case 0x14:
		*desc="Bank 1, Sensor1: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;
	case 0x15:
		*desc="Bank 1, Sensor2: Oxygen sensor voltage, Short term fuel trim";	  
		calc_os_val(A,B,value,unit);
		break;
	case 0x16:
		*desc="Bank 1, Sensor3: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;
	case 0x17:
		*desc="Bank 1, Sensor4: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;
	case 0x18:
		*desc="Bank 2, Sensor1: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;
	case 0x19:
		*desc="Bank 2, Sensor2: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;
	case 0x1A:
		*desc="Bank 2, Sensor3: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;
	case 0x1B:
		*desc="Bank 2, Sensor4: Oxygen sensor voltage, Short term fuel trim";
		calc_os_val(A,B,value,unit);
		break;

	case 0x1C:
	
		
		
		switch(A) {
		
			case 1:	temp1C = "OBD-II as defined by the CARB"; break;
			case 2:	temp1C = "OBD as defined by the EPA"; break;
			case 3:	temp1C = "OBD and OBD-II"; break;
			case 4:	temp1C = "OBD-I"; break;
			case 5:	temp1C = "Not OBD compliant"; break;
			case 6:	temp1C = "EOBD (Europe)"; break;
			case 7:	temp1C = "EOBD and OBD-II"; break;
			case 8:	temp1C = "EOBD and OBD"; break;
			case 9:	temp1C = "EOBD, OBD and OBD II"; break;
			case 10:	temp1C = "JOBD (Japan)"; break;
			case 11:	temp1C = "JOBD and OBD II"; break;
			case 12:	temp1C = "JOBD and EOBD"; break;
			case 13:	temp1C = "JOBD, EOBD, and OBD II"; break;

			case 17:	temp1C = "Engine Manufacturer Diagnostics (EMD)"; break;
			case 18:	temp1C = "Engine Manufacturer Diagnostics Enhanced (EMD+)"; break;
			case 19:	temp1C = "Heavy Duty On-Board Diagnostics (Child/Partial (HD OBD-C)"; break;
			case 20:	temp1C = "Heavy Duty On-Board Diagnostics (HD OBD)"; break;
			case 21:	temp1C = "World Wide Harmonized OBD (WWH OBD)"; break;

			case 23:	temp1C = "Heavy Duty Euro OBD Stage I without NOx control (HD EOBD-I)"; break;
			case 24:	temp1C = "Heavy Duty Euro OBD Stage I with NOx control (HD EOBD-I N)"; break;
			case 25:	temp1C = "Heavy Duty Euro OBD Stage II without NOx control (HD EOBD-II)"; break;
			case 26:	temp1C = "Heavy Duty Euro OBD Stage II with NOx control (HD EOBD-II N)"; break;
			case 28:	temp1C = "Brazil OBD Phase 1 (OBDBr-1)"; break;
			case 29:	temp1C = "Brazil OBD Phase 2 (OBDBr-2)"; break;
			case 30:	temp1C = "Korean OBD (KOBD)"; break;
			case 31:	temp1C = "India OBD I (IOBD I)"; break;
			case 32:	temp1C = "India OBD II (IOBD II)"; break;
			case 33:	temp1C = "Heavy Duty Euro OBD Stage VI (HD EOBD-IV)"; break;
			case 251:	case 252:	case 253:	case 254:	case 255:
				temp1C = "Not available for assignment (SAE J1939 special meaning)"; break;
				break;
			default:
				temp1C = "Reserved"; break;
				break;
		}
	
		*value=temp1C;
		*unit="";
		*desc="OBD standards this vehicle conforms to";
		break;
	case 0x1D:
		*value=std::to_string(data[0]); // BIT ENCODED TODO: DECODE
		*unit="";
		*desc="Oxygen sensors present";
		break;
	case 0x1E:
	  if(data[0] == 1) {
		*value=char*("PTO active)");
	  } else {
		*value=char*("PTO inactive)");
	  }
	  *unit="";
	  *desc="Auxillary input status";
	  break;  
	case 0x1F:
	  *value=std::to_string((data[0]*256) + data[1]);
	  *unit="s";
	  *desc="Run time since engine start";
	  break;  
	case 0x21:
	  *value=std::to_string((data[0]*256) + data[1]);
	  *unit="km";
	  *desc="Distance traveled with MIL on";
	  break;  
	case 0x22:
	  *value=std::to_string(( (data[0]*256.0) + data[1] ) * 0.079);
	  *unit="kPa";
	  *desc="Fuel rail pressure relative to manifold vacuum";
	  break;  
	case 0x23:
	  *value=std::to_string( ( (data[0]*256) + data[1] ) * 10);
	  *unit="kPa";
	  *desc="Fuel rail pressure";
	  break;  
	#define CALC_O2S \
	  *value = std::to_string ( ((data[0]*256.0) + data[1]) * 2.0 / 65535.0 ) + ", " + std::to_string( ((data[2]*256.0) + data[3]) * 8.0 / 65535.0 ) ; \
	  *unit = "n/a , V"; 
	case 0x24:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S1_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x25:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S2_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x26:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S3_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x27:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S4_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x28:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S5_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x29:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S6_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x2A:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S7_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	case 0x2B:
		calc_o2s(A,B,C,D, value, unit);
		*desc="O2S8_WR_lambda(1: Equivalence ratio, Voltage)";
		break;
	
	case 0x2C:
	  *value=std::to_string(data[0]*100.0 / 255.0);
	  *unit="%";
	  *desc="Commanded EGR";
	  break;  
	case 0x2D:
	  *value=std::to_string((data[0]-128.0) * 100.0 / 255.0);
	  *unit="%";
	  *desc="EGR Error";
	  break;
	case 0x2E:
	  *value=std::to_string(data[0] * 100.0 / 255.0);
	  *unit="%";
	  *desc="Commanded evaporative purge";
	  break;  
	case 0x2F:
	  *value=std::to_string(data[0] * 100.0 / 255.0);
	  *unit="%";
	  *desc="Fuel level input";
	  break;    	
	case 0x30:
		*value=std::to_string(data[0]);
		*unit="";
		*desc="";
		break;
	case 0x31:
		*value=std::to_string(data[0]);
		*unit="";
		*desc="";
		break;
	case 0x32: // TWOs Complement
		*value=std::to_string(data[0]);
		*unit="";
		*desc="";
		break;
	case 0x33:
		*value=std::to_string(data[0]);
		*unit="";
		*desc="";
		break;
	#define CALC_O2SB \
	  *value = std::to_string ( ((data[0]*256.0) + data[1]) * 2.0 / 65535.0 ) + ", " + std::to_string( (((data[2]*256.0) + data[3]) / 256.0) - 128.0 ) ; \
	  *unit = "n/a , mA"; 	
	case 0x34:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S1_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x35:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S2_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x36:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S3_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x37:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S4_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x38:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S5_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x39:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S6_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x3A:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S7_WR_lambda(1: Equivalence ratio, Current)";
		break;
	case 0x3B:
		calc_o2s_b(A,B,C,D, value, unit);
		*desc="O2S8_WR_lambda(1: Equivalence ratio, Current)";
		break;
	#define CALC_CAT_TEMP \
	  *value = std::to_string ( (((data[0]*256)+data[1])/10) - 40 ); \
	  *unit = "�C"; 		
	case 0x3C:
		calc_cat_temp(A,B,value,unit);
		*desc="Catalyst Temperature Bank 1, Sensor 1";
		break;
	case 0x3D:
		calc_cat_temp(A,B,value,unit);
		*desc="Catalyst Temperature Bank 2, Sensor 1";
		break;
	case 0x3E:
		calc_cat_temp(A,B,value,unit);
		*desc="Catalyst Temperature Bank 1, Sensor 2";
		break;
	case 0x3F:
		calc_cat_temp(A,B,value,unit);
		*desc="Catalyst Temperature Bank 2, Sensor 2";
		break;
	case 0x41:
		*value=std::to_string(data[0]); // BIT ENCODED
		*unit="";
		*desc="Monitor status this drive cycle";
		break;
	case 0x42:
		*value=std::to_string(((A*256.0)+B)/1000.0);
		*unit="V";
		*desc="Control module voltage";
		break;
	case 0x43:
		*value=std::to_string(((A*256.0)+B)*100.0/255.0);
		*unit="%";
		*desc="Absolute load value";
		break;
	case 0x44:
		*value=std::to_string(((A*256.0)+B)/32768.0);
		*unit="";
		*desc="Fuel/Air commanded equivalence ratio";
		break;
	case 0x45:
		calc_percent(data[0], value, unit);
		*desc="Relative throttle position";
		break;
	case 0x46:
		*value=std::to_string(A-40.0);
		*unit="�C";
		*desc="Ambient air temperature";
		break;
	case 0x47:
		calc_percent(data[0], value, unit);
		*desc="Absolute throttle position B";
		break;
	case 0x48:
		calc_percent(data[0], value, unit);
		*desc="Absolute throttle position C";
		break;
	case 0x49:
		calc_percent(data[0], value, unit);
		*desc="Accelerator pedal position D";
		break;
	case 0x4A:
		calc_percent(data[0], value, unit);
		*desc="Accelerator pedal position E";
		break;
	case 0x4B:
		calc_percent(data[0], value, unit);
		*desc="Accelerator pedal position F";
		break;
	case 0x4C:
		calc_percent(data[0], value, unit);
		*desc="Commanded throttle actuator";
		break;
	case 0x4D:
		*value=std::to_string((A*256.0)+B);
		*unit="min";
		*desc="Time run with MIL on";
		break;
	case 0x4E:
		*value=std::to_string((A*256.0)+B);
		*unit="min";
		*desc="Time since trouble codes cleared";
		break;
	case 0x4F:
		*value=std::to_string(A) + ", " + std::to_string(B) + ", " + std::to_string(C) + ", " + std::to_string(D*10.0);
		*unit="n/a, V, mA, kPa";
		*desc="Maximum value for equivalence ratio, oxygen sensor voltage, oxygen sensor current, and intake manifold absolute pressure";
		break;	
	case 0x50:
		*value=std::to_string(A*10) + ", " + std::to_string(B) + ", " + std::to_string(C) + ", " + std::to_string(D);
		*unit="g/s";
		*desc="Maximum value for air flow rate from mass air flow sensor";
		break;	
	case 0x51:
		*value=std::to_string(data[0]); // uint32_t ENCODED
		*unit="";
		*desc="Fuel Type";
		break;	
	case 0x52:
		calc_percent(data[0], value, unit);
		*desc="Ethanol fuel %	";
		break;	
	case 0x53:
		*value=std::to_string(((A*256.0)+B)/200.0);
		*unit="kPa";
		*desc="Absolute Evap system Vapor Pressure";
		break;	
	case 0x54:
		*value=std::to_string(((A*256.0)+B)-32767.0);
		*unit="";
		*desc="Evap system vapor pressure	";
		break;	
		#define CALC_STSOS \
		*value=std::to_string((A-128.0)*100.0/128.0) + ", " + std::to_string(std::to_string((B-128.0)*100.0/128.0)); \
		*unit="%, %";
	case 0x55:
		calc_stsos(A,B, value, unit);
		*desc="Short term secondary oxygen sensor trim bank 1 and bank 3";
		break;	
	case 0x56:
		calc_stsos(A,B, value, unit);
		*desc="Long term secondary oxygen sensor trim bank 1 and bank 3";
		break;	
	case 0x57:
		calc_stsos(A,B, value, unit);
		*desc="Short term secondary oxygen sensor trim bank 2 and bank 4";
		break;	
	case 0x58:
		calc_stsos(A,B, value, unit);
		*desc="Long term secondary oxygen sensor trim bank 2 and bank 4";
		break;
	case 0x59:
		*value=std::to_string(((A*256.0)+B) * 10.0);
		*unit="kPa";
		*desc="Fuel rail pressure (absolute)";
		break;
	case 0x5A:
		calc_percent(data[0], value, unit);
		*desc="Relative accelerator pedal position";
		break;
	case 0x5B:
		calc_percent(data[0], value, unit);
		*desc="Hybrid battery pack remaining life	";
		break;
	case 0x5C:
		calc_temp(A, value, unit);
		*desc="Engine oil temperature";
		break;
	case 0x5D:
		*value=std::to_string((((A*256.0)+B)-26880.0)/128.0);
		*unit="�";
		*desc="Fuel injection timing";
		break;
	case 0x5E:
		*value=std::to_string(((A*256.0)+B)*0.05);
		*unit="L/h";
		*desc="Engine fuel rate	";
		break;
	case 0x5F:
		*value=std::to_string(A); // bit encoded
		*unit="";
		*desc="Emission requirements to which vehicle is designed";
		break;
	#define CALC_MOD_PERCENT \
		*value=std::to_string(data[0] - 125); \
		*unit="%";
	case 0x61:
		CALC_MOD_PERCENT
		*desc="Driver's demand engine - percent torque";
		break;
	case 0x62:
		CALC_MOD_PERCENT
		*desc="Actual engine - percent torque";
		break;
	case 0x63:
		*value=std::to_string(A*256.0+B);
		*unit="";
		*desc="Engine reference torque";
		break;
	case 0x64:
		*value=std::to_string(A-125) + ", " + std::to_string(B-125) + ", " + std::to_string(C-125) + ", " + std::to_string(D-125) + ", " + std::to_string(E-125);
		*unit="%";
		*desc="Engine percent torque data - IDLE, Engine Pouint32_t 1, EP2, EP3, EP4";
		break;	
	case 0x65:
		*value=char*("TODO"); // TODO: BIT ENCODED
		*unit="";
		*desc="Auxiliary input / output supported	";
		break;	
	case 0x66:
		*value=std::to_string(A) + ", " + std::to_string(B) + ", " + std::to_string(C) + ", " + std::to_string(D) + ", " + std::to_string(E); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Mass air flow sensor";
		break;	
	case 0x67:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Engine coolant temperature";
		break;	
	case 0x68:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Intake air temperature sensor";
		break;	
	case 0x69:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Commanded EGR and EGR Error";
		break;	
	case 0x6A:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Commanded Diesel intake air flow control and relative intake air flow position";
		break;	
	case 0x6B:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Exhaust gas recirculation temperature";
		break;	
	case 0x6C:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Commanded throttle actuator control and relative throttle position";
		break;	
	case 0x6D:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Fuel pressure control system";
		break;	
	case 0x6E:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Injection pressure control system";
		break;	
	case 0x6F:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Turbocharger compressor inlet pressure";
		break;	
	case 0x70:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Boost pressure control";
		break;
	case 0x71:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Variable Geometry turbo (VGT control)";
		break;
	case 0x72:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Wastegate control";
		break;
	case 0x73:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Exhaust pressure";
		break;
	case 0x74:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Turbocharger RPM";
		break;
	case 0x75:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Turbocharger temperature";
		break;
	case 0x76:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Turbocharger temperature";
		break;
	case 0x77:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Charge air cooler temperature (CACT)";
		break;
	case 0x78:
		*value=char*("TODO"); // special pid
		*unit="";
		*desc="Exhaust Gas temperature (EGT Bank 1)";
		break;
	case 0x79:
		*value=char*("TODO"); // special pid
		*unit="";
		*desc="Exhaust Gas temperature (EGT Bank 2)";
		break;
	case 0x7A:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Diesel particulate filter (DPF)";
		break;
	case 0x7B:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Diesel particulate filter (DPF)";
		break;
	case 0x7C:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Diesel Particulate filter (DPF temperature)";
		break;
	case 0x7D:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="NOx NTE control area status";
		break;
	case 0x7E:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="PM NTE control area status	";
		break;
	case 0x7F:
		*value=char*("TODO"); // NO FORMULA AVAILABLE YET
		*unit="";
		*desc="Engine run time";
		break;	
    // usw.
  }
  /*
  switch(pid) {
	
	case 0x01:
		*value="aa";
		break;
		
	case 0x02:
		*value="bb";
		break;
	
	case 0x03:
		*value="cc";
		break;	
		
	case 0x04:
		*value="dd";
		break;		
	
	case 0x05:
		*value="ee";
		break;	
	
	case 0x06:
		*value="ff";
		break;	
	
	
  }	  
  */
}


#endif
#endif