/*
  elm.h - Library for communicating with an ELM327 Shield.
  Created by Florian Beck, Viktor Pavlovic, Simon Schuster.
  Hochschule der Medien, Stuttgart.
  Stuttgart Media University
*/

#ifndef ELM_h
#define ELM_h

#include <string>
#include <mbed.h>

typedef enum  {
  Automatic = '0',
  SAE_J1850_PWM_41_6_kbaud = '1',
  SAE_J1850_VPW_10_4_kbaud = '2',
  ISO_9141_2__5_baud_init_10_4_kbaud = '3',
  ISO_14230_4_KWP_5_baud_init_10_4_kbaud = '4',
  ISO_14230_4_KWP_fast_init_10_4_kbaud = '5',
  ISO_15765_4_CAN_11_bit_ID_500_kbaud = '6',
  ISO_15765_4_CAN_29_bit_ID_500_kbaud = '7',
  ISO_15765_4_CAN_11_bit_ID_250_kbaud = '8',
  ISO_15765_4_CAN_29_bit_ID_250_kbaud = '9',
  SAE_J1939_CAN_29_bit_ID_250_kbaud = 'A',
  USER1_CAN_11_bit_ID_125_kbaud = 'B',
  USER2_CAN_11_bit_ID_50_kbaud = 'C'
} protocol_t;

class ELM {
  private:
    uint8_t _RX;
    uint8_t _TX;
    uint32_t _UARTBAUD;

    RawSerial *UART;
    const char* AT(const char* Cmd);
    char* pid(uint8_t id);
    void update_available_pidset(uint8_t set);
    void update_available_pids();    
    bool available_pids[256];
    bool available_pids_checked = false;
    void parsePID(uint8_t pid, uint8_t data[], char* value, char* unit, char *desc);
    uint8_t hex2uint8_t(char* in, uint8_t pos, char* separator = "");
  
  
  public:  
    ELM(RawSerial* uart) : UART(uart) {};
    
    void flush();
    void qAT(const char *msg);
    void init();
    bool protocol(protocol_t p);
    bool reset();
    std::string get_available_pids();
    bool pid_available(uint8_t pid);
    const char* get_pid_rawdata(uint8_t pid);
    const char* get_pid_data(uint8_t pid);
    const char* get_pid_unit(uint8_t pid);
    const char* get_pid_desc(uint8_t pid);
    const char* get_vin();
    const char* get_ecu();
    const char* get_voltage();
    const char* get_protocol();
    const char* get_dtc();
    bool clear_dtc();
};

#endif